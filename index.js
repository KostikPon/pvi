class Student {
    constructor(group, name, gender, birthday, status) {
        this.group = group;
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.status = status; // Залишаємо значення як булівське
    }
}

// Масив студентів
let students = [
    new Student("PZ-27", "James Bond", "Male", "2000-01-01", true), // Змінено значення на true
    new Student("PZ-23", "Yohanson Scarlet", "Female", "1999-12-31", false), // Змінено значення на false
    new Student("KN-24", "Mugiwara Luffy", "Male", "2001-05-15", true), // Змінено значення на true
    new Student("KB-21", "Roronua Zoro", "Male", "2000-06-20", false) // Змінено значення на false
];
let addStudentButton = document.getElementById("addStudents");
let table = document.getElementById("tableOfStudents");
let studentIndex = 0;

addStudentButton.addEventListener('click', function (){
    let currentStudent = students[studentIndex];
    let newRow = document.createElement("tr");

    let checkboxCell = document.createElement("td");
    let checkboxInput = document.createElement("input");
    checkboxInput.setAttribute("type", "checkbox");
    checkboxCell.appendChild(checkboxInput);
    newRow.appendChild(checkboxCell);

    for (let i = 0; i < 4; i++) {
        let cell = document.createElement("td");
        cell.textContent = Object.values(currentStudent)[i];
        newRow.appendChild(cell);
    }

    let statusCell = document.createElement("td");
    let statusIcon = document.createElement("img");
    if (currentStudent.status) {
        statusIcon.setAttribute("src", "images/green_circle.svg");
        statusIcon.setAttribute("alt", "Online");
    } else {
        statusIcon.setAttribute("src", "images/grey_circle.svg");
        statusIcon.setAttribute("alt", "Offline");
    }
    statusCell.appendChild(statusIcon);
    newRow.appendChild(statusCell);

    let optionsCell = document.createElement("td");

    let editButton = document.createElement("button");
    editButton.style.margin = "5px"; // Додаємо margin
    let editIcon = document.createElement("img");
    editIcon.setAttribute("src", "images/pencil.svg");
    editIcon.setAttribute("alt", "Edit");
    editButton.appendChild(editIcon);
    optionsCell.appendChild(editButton);

    let deleteButton = document.createElement("button");
    deleteButton.style.margin = "5px";
    deleteButton.addEventListener("click", function() {
        let row = this.parentNode.parentNode;
        row.parentNode.removeChild(row);
    });
    let deleteIcon = document.createElement("img");
    deleteIcon.setAttribute("src", "images/trash3.svg");
    deleteIcon.setAttribute("alt", "Delete");
    deleteButton.appendChild(deleteIcon);
    optionsCell.appendChild(deleteButton);

    newRow.appendChild(optionsCell);
    table.appendChild(newRow);
    studentIndex++;
    if (studentIndex >= students.length) {
        studentIndex = 0;
    }
})

document.getElementById("bell").addEventListener("click", function() {
    let bellNotification = document.querySelector("#bell_notification .container");
    let user = document.querySelector("#user_image .container");

    if (bellNotification.style.display === "none" || bellNotification.style.display === "") {
        bellNotification.style.display = "block";
        user.style.display = "none"; // Приховуємо віконечко зображення користувача
    } else {
        bellNotification.style.display = "none";
    }
});

document.getElementById("user_image").addEventListener("click", function() {
    let user = document.querySelector("#user_image .container");
    let bellNotification = document.querySelector("#bell_notification .container");

    if (user.style.display === "none" || user.style.display === "") {
        user.style.display = "block";
        bellNotification.style.display = "none"; // Приховуємо віконечко дзвіночка
    } else {
        user.style.display = "none";
    }
});
